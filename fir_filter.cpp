# include "fir_filter.h"
# include <stdio.h>
# include <math.h>
# include <stdlib.h>

int main(int argc, char *argv []) {
	FirFilter fir, *fp;
	fp = &fir;
	FirFilter_init(fp);
	//build the sinus function
	const float pi=3.14159;
	const float f=1; // frequenz
	float sinus_value;
	int n=100; //sampling rate
	//noise
	float noise;
	float min=-0.15;
	float max=0.15;
	
	for(int i=0; i<3*n; i++) {
		noise = min+((float)rand()/(float)RAND_MAX)*max-min;
		sinus_value= sin(2*pi*f*i/n)+noise;
		printf("%7.4f;", sinus_value);
		//filtered value
		printf("%7.4f\n", FirFilter_calc(fp,sinus_value));
	}
		
}

void FirFilter_init(FirFilter *fir) {
  //clear buffer
  for (int i=0; i<FIR_FILTER_LENGTH; i++) {
		fir->buffer[i] = 0.0f;		
    }
  //clear output value
  fir->out = 0.0f;
	
}

float FirFilter_calc(FirFilter *fir, float inputValue){
	//impule respone low pass filter
  
	//shift buffer values right
  for(int i = FIR_FILTER_LENGTH-1; i>0; i--) {
    fir->buffer[i] = fir->buffer[i-1];
  }
	//insert input
  fir->buffer[0]=inputValue;
	//multiply buffer with the imp.respone and summarize
  for(int i = 0; i < FIR_FILTER_LENGTH; i++) {
    fir->out += fir->buffer[i] * impulseResponse[i];
  }

  return fir->out;



}