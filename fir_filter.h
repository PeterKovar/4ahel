#ifndef FIR_FILTER_H
#define FIR_FILTER_H

#include <stdint.h>
# define FIR_FILTER_LENGTH 11

typedef struct{
	float buffer[FIR_FILTER_LENGTH];    //circular buffer array
	uint8_t bufferIndex;   //Index of the circular buffer array
	float out;            //Output of the circular buffer array
	
} FirFilter;	
//function prototypes
void FirFilter_init(FirFilter *fir);
float FirFilter_calc(FirFilter *fir, float inputValue);
float impulseResponse[FIR_FILTER_LENGTH] = 
	{0.0637,0,-0.1061,0,0.3183,0.5,0.3183,0,-0.1061,0,0.0637};

#endif